# ansible==2.10.5
# ansible-base==2.10.5
# ansible-lint==4.3.7
# arrow==0.17.0
# bcrypt==3.2.0
# binaryornot==0.4.4
# Cerberus==1.3.2
# certifi==2020.12.5
# cffi==1.14.4
# chardet==4.0.0
# click==7.1.2
# click-completion==0.5.2
# click-help-colors==0.9
# colorama==0.4.4
# commonmark==0.9.1
# cookiecutter==1.7.2
# cryptography==3.3.1
# docker==4.4.1
# enrich==1.2.6
# flake8==3.8.4
# idna==2.10
# Jinja2==2.11.2
# jinja2-time==0.2.0
# jmespath==0.10.0
# MarkupSafe==1.1.1
# molecule==3.2.2
# molecule-docker==0.2.4
# packaging==20.8
# paramiko==2.7.2
# pathspec==0.8.1
# pluggy==0.13.1
# poyo==0.5.0
# pycparser==2.20
# Pygments==2.7.4
# PyNaCl==1.4.0
# pyparsing==2.4.7
# python-dateutil==2.8.1
# python-slugify==4.0.1
# PyYAML==5.4.1
# requests==2.25.1
# rich==9.9.0
# ruamel.yaml==0.16.12
# ruamel.yaml.clib==0.2.2
# shellingham==1.3.2
# six==1.15.0
# subprocess-tee==0.2.0
# text-unidecode==1.3
# typing-extensions==3.7.4.3
# urllib3==1.26.2
# websocket-client==0.57.0
# yamllint==1.25.0


ansible==6.1.0
ansible-compat==2.2.0
ansible-core==2.13.2
ansible-lint==6.4.0
arrow==1.2.2
attrs==22.1.0
binaryornot==0.4.4
bracex==2.3.post1
Cerberus==1.3.2
certifi==2022.6.15
cffi==1.15.1
chardet==5.0.0
charset-normalizer==2.1.0
click==8.1.3
click-help-colors==0.9.1
commonmark==0.9.1
cookiecutter==2.1.1
cryptography==37.0.4
docker==5.0.3
enrich==1.2.7
flake8==4.0.1
idna==3.3
iniconfig==1.1.1
Jinja2==3.1.2
jinja2-time==0.2.0
jmespath==1.0.1
jsonschema==4.8.0
MarkupSafe==2.1.1
mccabe==0.6.1
molecule==4.0.1
molecule-docker==2.0.0
packaging==21.3
pathspec==0.9.0
pluggy==1.0.0
py==1.11.0
pycodestyle==2.8.0
pycparser==2.21
pyflakes==2.4.0
Pygments==2.12.0
pyparsing==3.0.9
pyrsistent==0.18.1
pytest==7.1.2
pytest-testinfra==6.8.0
python-dateutil==2.8.2
python-slugify==6.1.2
PyYAML==6.0
requests==2.28.1
resolvelib==0.8.1
rich==12.5.1
ruamel.yaml==0.17.21
ruamel.yaml.clib==0.2.6
six==1.16.0
subprocess-tee==0.3.5
text-unidecode==1.3
tomli==2.0.1
urllib3==1.26.11
wcmatch==8.4
websocket-client==1.3.3
yamllint==1.27.1