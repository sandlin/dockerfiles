#!/usr/bin/env python
import sys
import argparse
import os
import jinja2
import configparser
from dataclasses import dataclass

@dataclass
class DockerImageToBuild:
    """
        Class for a subdir which has a Dockerfile & version.cfg
        Uses the dataclass module
        REF: https://docs.python.org/3/library/dataclasses.html
    """
    name: str
    path: str
    version: str = "latest"

    def load_version(self):
        '''
        Load the version value(s) from the version.cfg
        Assign this value into the current object.
        :return: n/a
        '''
        #print(self.path)
        cfgfile = os.path.join(self.path, 'version.cfg')
        if not os.path.isfile(cfgfile):
            raise FileNotFoundError
        cp = configparser.ConfigParser()
        mycfg = cp.read(cfgfile)
        try:
            self.version = cp['latest']['version']
        except KeyError as e:
            raise e

def gen_src_list():
    '''
        1) Get a list of all directories under src
        2) Iterate through this list of subdirs.
        3) Create a Dockerfile object representing each.
        4) Tell the object to load the version from the internal config.
        :return: a collection of DockerImageToBuild objects.
    '''
    dockerfiles = []
    src_dir = "./src"
    src_dirs = os.listdir(src_dir)
    for sd in src_dirs:
        # Read the version from the file
        df = DockerImageToBuild(name=sd, path=os.path.join(src_dir, sd))
        df.load_version()
        dockerfiles.append(df)
    return dockerfiles


def parse_args(args):
    """
    Parse command line arguments

    :param args: command args

    :returns: argparse args
    """
    parser = argparse.ArgumentParser(description="Generate dynamic yml")
    parser.add_argument('--outputfile', type=str, help='The name of the file to generate', default='downstream_pipeline.yml')
    pargs = parser.parse_args(args)
    return pargs

def main(**kwargs):
    '''
    Generate the source object list then pass it to the jinja2 template to render the pipeline.
    :param argv: n/a
    :return: n/a
    '''
    outputfile = kwargs.get('outputfile')
    docker_src_list = gen_src_list()
    if os.getenv('CI_COMMIT_BRANCH') is not None:
        rendered = jinja2.Template(open('./templates/artifact_gitlab_ci.yml.j2').read()).render(docker_src_list=docker_src_list, CI_COMMIT_BRANCH=os.getenv('CI_COMMIT_BRANCH'))
    else:
        rendered = jinja2.Template(open('./templates/artifact_gitlab_ci.yml.j2').read()).render(docker_src_list=docker_src_list)
    fp = open(outputfile, 'w')
    n = fp.write(rendered)
    fp.close()

if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    sys.exit(main(**vars(args)))
